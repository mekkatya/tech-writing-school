.. _ci_cd:

Understanding Continuous Methodologies
======================================

GitLab documentation `defines <https://docs.gitlab.com/ee/ci/introduction/index.html>`_ continuous method as follows:

   With the continuous method of software development, you continuously build, test, and deploy iterative code changes.
   This iterative process helps reduce the chance that you develop new code based on buggy or failed previous versions.
   With this method, you strive to have less human intervention or even no intervention at all,
   from the development of new code until its deployment.

Continuous integration (CI), continuous delivery (CD), and continuous deployment (CD) are essential for the development of both software and documentation.
Below you will find out about the CI/CD pipelines created for `tech-writing-school repository <https://gitlab.com/outoftardis/tech-writing-school>`_.

.. seealso::

   - `More about CI/CD <https://docs.gitlab.com/ee/ci/>`_

CI/CD Pipelines
***************

This repository uses these methodologies to test and deploy documentation. The highlighted lines in ``.gitlab-ci.yml``
correspond to the deployment (``pages``) and test (``test``) processes:

.. literalinclude:: ../../.gitlab-ci.yml
   :language: yaml
   :emphasize-lines: 3,23
   :caption: ``.gitlab-ci.yml``: CI/CD for tech-writing-school repository

Deploying GitLab Pages
**********************

Take a look at the ``pages`` pipeline below.

It runs the steps in the ``script`` but ``only`` if the changes to the ``main`` branch are introduced.
The last step in the script consist in moving the built documenation to the ``public`` folder.
To deploy a website, the pipeline takes the ``artifact`` from the ``public`` folder.

.. literalinclude:: ../../.gitlab-ci.yml
   :language: yaml
   :lines: 3-21
   :caption: ``.gitlab-ci.yml``: Deploying GitLab Pages

.. seealso::

   - `Create a Pages website by using a CI/CD template <https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_ci_cd_template.html>`_

Testing Documentation Build
***************************

Take a look at the ``test`` pipeline below.

It runs the steps in the ``script`` but ``only`` when a ``merge request`` is created. Note that this script is not the same as in ``pages``.
The build command (``sphinx-build -W --keep-going -n source build``) introduces options that treat warnings as errors.
This way, the pipeline will pass only if there are no problems in documentation.

.. literalinclude:: ../../.gitlab-ci.yml
   :language: yaml
   :lines: 23-36
   :caption: ``.gitlab-ci.yml``: Testing documentation build

.. seealso::

   - :ref:`contribute_to_open_source`
   - `Merge Requests in GitLab docs <https://docs.gitlab.com/ee/user/project/merge_requests/>`_
   - `sphinx-build options <https://www.sphinx-doc.org/en/master/man/sphinx-build.html>`_