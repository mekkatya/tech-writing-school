.. _work_with_sphinx:

Working with Sphinx
===================

Sphinx is a Python Documentation Generator that supports multiple output formats.

Installation
************

Sphinx is a Python package that requires Python 3.6+ installed on your machine.

To install Sphinx, refer to its `Installation instructions <https://www.sphinx-doc.org/en/master/usage/installation.html>`_.

Get Started
***********

After installation, go through a `Quick Start <https://www.sphinx-doc.org/en/master/usage/quickstart.html>`_ instructions
set up your first project.

Refer to :ref:`use_rst` for details about the markup used in Sphinx projects.

.. seealso::

   - `Projects that generate their documentation using Sphinx <https://www.sphinx-doc.org/en/master/examples.html>`_
   - `Gallery: Sphinx Themes <https://sphinx-themes.org/>`_
   - `Built-in extensions <https://www.sphinx-doc.org/en/master/usage/extensions/index.html#builtin-sphinx-extensions>`_
   - `sphinx-contrib: a collection of Sphinx extensions maintained by their respective authors <https://github.com/sphinx-contrib>`_
   - `Sphinx documentation <https://www.sphinx-doc.org/en/master/index.html>`_
